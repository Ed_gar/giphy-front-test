const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  purge: ['./index.html', './src/**/*.{html,js,vue}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    screens: {
      'xs': '475px',
      ...defaultTheme.screens,
    },
    colors: {
      'dark-gray': '#2e2e2e',
      'overlay': '#000000cc',
      'metal': '#a6a6a6',
      'purple': '#7e3af2'
    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}